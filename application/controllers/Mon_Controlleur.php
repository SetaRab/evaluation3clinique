<?php 	
defined('BASEPATH') OR exit('No direct script access allowed');

class Mon_Controlleur extends CI_Controller{
	public function __construct(){
        parent::__construct();
    }
    public function index(){
        $this->load->view('welcom');
    }   

    public function deconnexion(){
        session_destroy();
        $this->index();
    }

    public function authentification(){
        $email=$_POST['email'];
        $mdp=$_POST['mdp'];
        $this->load->model('Mon_Model');
        $data=$this->Mon_Model->authentificate($email,$mdp);
        if ($data==true) {
            if ($data[0]['statut']==0) {
                $this->adminAccueil();
            }else {
                $this->utilisateurAccueil();
            }
        }
    }

    public function adminAccueil(){
        $data['page']="accueilAdmin";
        $this->load->view('pageAdmin',$data);
    }

    public function utilisateurAccueil(){
        $data['page']="accueilUtil";
        $this->load->view('pageUtil',$data);
    }

    public function patient(){
        $data=array();
        $data['page']="patient";
        $this->load->model('Mon_Model');
        $patient=$this->Mon_Model->getPatient();
        $genre=$this->Mon_Model->getGenre();
        $data['patient']=$patient;
        $data['genre']=$genre;
        $this->load->view('pageAdmin',$data);
    }

    public function addPatient(){
        $nom=$_POST['nom'];
        $prenom=$_POST['prenom'];
        $datenaissance=$_POST['datenaissance'];
        $idGenre=$_POST['idGenre'];
        $contact=$_POST['contact'];
        $remboursement=$_POST['remboursement'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->addPatient($nom,$prenom,$datenaissance,$idGenre,$contact,$remboursement);
        $this->patient();
    }

    public function updatePatient(){
        $idPatient=$_POST['idPatient'];
        $nom=$_POST['nom'];
        $prenom=$_POST['prenom'];
        $datenaissance=$_POST['datenaissance'];
        $idGenre=$_POST['idGenre'];
        $contact=$_POST['contact'];
        $remboursement=$_POST['remboursement'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->updatePatient($idPatient,$nom,$prenom,$datenaissance,$idGenre,$contact,$remboursement);
        $this->patient();
    }

    public function deletePatient(){
        $idPatient=$_GET['idPatient'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->deletePatient($idPatient);
        $this->patient();
    }


    public function acte(){
        $data=array();
        $data['page']="actes";
        $this->load->model('Mon_Model');
        $acte=$this->Mon_Model->getActe();
        $data['acte']=$acte;
        $this->load->view('pageAdmin',$data);
    }

    public function addActe(){
        $nomActe=$_POST['nomActe'];
        $budget=$_POST['budget'];
        $code=$_POST['code'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->addActe($nomActe,$budget,$code);
        $this->acte();
    }

    public function updateActe(){
        $idTypeActe=$_POST['idTypeActe'];
        $nomActe=$_POST['nom'];
        $budget=$_POST['budget'];
        $code=$_POST['code'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->updateActe($idTypeActe,$nomActe,$budget,$code);
        $this->acte();
    }

    public function deleteActe(){
        $idTypeActe=$_GET['idTypeActe'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->deleteActe($idTypeActe);
        $this->acte();
    }



    public function depense(){
        $data=array();
        $data['page']="depense";
        $this->load->model('Mon_Model');
        $depense=$this->Mon_Model->getDepense();
        $data['depense']=$depense;
        $this->load->view('pageAdmin',$data);
    }

    public function addDepense(){
        $nomDepense=$_POST['nomDepense'];
        $budget=$_POST['budget'];
        $code=$_POST['code'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->addDepense($nomDepense,$budget,$code);
        $this->depense();
    }

    public function updateDepense(){
        $idTypeDepense=$_POST['idTypeDepense'];
        $nomDepense=$_POST['nomDepense'];
        $budget=$_POST['budget'];
        $code=$_POST['code'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->updateDepense($idTypeDepense,$nomDepense,$budget,$code);
        $this->depense();
    }

    public function deleteDepense(){
        $idTypeDepense=$_GET['idTypeDepense'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->deleteDepense($idTypeDepense);
        $this->depense();
    }


    public function utilDepense($erreur=''){
        $data=array();
        $data['page']="utilDepense";
        $this->load->model('Mon_Model');
        $utilDepense=$this->Mon_Model->getUtilDepense();
        $depense=$this->Mon_Model->getDepense();
        $mois=$this->Mon_Model->getMois();
        $data['utilDepense']=$utilDepense;
        $data['depense']=$depense;
        $data['erreur']=$erreur;
        $data['mois']=$mois;
        $this->load->view('pageUtil',$data);
    }

    public function addUtilDepense(){
        $code=$_POST['code'];
        $jour=$_POST['jour'];
        $annee=$_POST['annee'];
        $montant=$_POST['montant'];
        $this->load->model('Mon_Model');
        $idTypeDepense= $this->Mon_Model->getIdCode($code);
        $erreur="";
        if (isset($_POST['mois'])) {
            $mois=$_POST['mois'];
            $val="";
            $i=0;
            foreach ($mois as $key) {
                if (checkdate($key,$jour,$annee)==false) {
                    $val=$val."1";
                    $date[$i]=$annee.'-'.$key.'-'.$jour;
                    $i++;
                    
                } else {
                    $val=$val."0";
                }
            }
            if ($montant<0) {
                $erreur=$erreur."<div class='alert alert-danger bg-danger text-light border-0 alert-dismissible fade show' role='alert'>Montant invalide
                                <button type='button' class='btn-close btn-close-white' data-bs-dismiss='alert' aria-label='Close'></button>
                            </div>";
                if (strpos($val,"1")!==false) {
                    for ($j=0; $j < count($date); $j++) { 
                        $erreur=$erreur."<div class='alert alert-danger bg-danger text-light border-0 alert-dismissible fade show' role='alert'>Erreur sur la date ";
                        $erreur=$erreur."".$date[$j]." ";
                        $erreur=$erreur."<button type='button' class='btn-close btn-close-white' data-bs-dismiss='alert' aria-label='Close'></button>
                                        </div>";
                    }
                }else{
                    if (count($idTypeDepense)!="") {
                        foreach ($mois as $variable) {
                            $dateDepense=$annee.'-'.$variable.'-'.$jour;
                            $this->Mon_Model->addUtilDepense($idTypeDepense,$dateDepense,$montant);
                        }
                    } else {
                        $erreur=$erreur."<div class='alert alert-danger bg-danger text-light border-0 alert-dismissible fade show' role='alert'>Code Invalide
                                <button type='button' class='btn-close btn-close-white' data-bs-dismiss='alert' aria-label='Close'></button>
                            </div>";
                    }
                }
            } 
            else {
                if (strpos($val,"1")!==false) {
                    for ($j=0; $j < count($date); $j++) { 
                        $erreur=$erreur."<div class='alert alert-danger bg-danger text-light border-0 alert-dismissible fade show' role='alert'>Erreur sur la date ";
                        $erreur=$erreur."".$date[$j]." ";
                        $erreur=$erreur."<button type='button' class='btn-close btn-close-white' data-bs-dismiss='alert' aria-label='Close'></button>
                                        </div>";
                    }
                }else{
                    if (count($idTypeDepense)!="") {
                        foreach ($mois as $variable) {
                            $dateDepense=$annee.'-'.$variable.'-'.$jour;
                            $this->Mon_Model->addUtilDepense($idTypeDepense,$dateDepense,$montant);
                        }
                    } else {
                        $erreur=$erreur."<div class='alert alert-danger bg-danger text-light border-0 alert-dismissible fade show' role='alert'>Code Invalide
                                <button type='button' class='btn-close btn-close-white' data-bs-dismiss='alert' aria-label='Close'></button>
                            </div>";
                    }
                }
            }
        }
        $this->utilDepense($erreur);
    }

    public function updateUtilDepense(){
        $idDepense=$_POST['idDepense'];
        $idTypeDepense=$_POST['idTypeDepense'];
        $dateDepense=$_POST['dateDepense'];
        $montant=$_POST['montant'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->updateUtilDepense($idDepense,$idTypeDepense,$dateDepense,$montant);
        $this->utilDepense();
    }

    public function deleteUtilDepense(){
        $idDepense=$_GET['idDepense'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->deleteUtilDepense($idDepense);
        $this->utilDepense();
    }


    public function tableauDeBord(){
        $data=array();
        $data['page']="tableauDeBord";
        $idmois=isset($_GET['idMois']) ? $_GET['idMois'] : "";
        $annee=isset($_GET['annee']) ? $_GET['annee'] : "";
        $this->load->model('Mon_Model');
        $tabdebord=$this->Mon_Model->tableauDeBord($idmois,$annee);
        $tabdebordrecette=$this->Mon_Model->tableauDeBordRecette($idmois,$annee);
        $mois=$this->Mon_Model->getMois();  
        $data['tabdebordrecette']=$tabdebordrecette;
        $data['tabdebord']=$tabdebord;
        $data['mois']=$mois;
        $data['annee']=$annee;
        $this->load->view('pageAdmin',$data);
    }

    public function utilRecette(){
        $data=array();
        $data['page']="utilRecette";
        $this->load->model('Mon_Model');
        $acte=$this->Mon_Model->getActe();
        $patient=$this->Mon_Model->getPatient();
        $data['patient']=$patient;
        $data['acte']=$acte;
        $this->load->view('pageUtil',$data);
    }
    
    public function addRecette(){
        $idTypeActe=$_POST['idTypeActe'];
        $dateActe=$_POST['dateActe'];
        $montant=$_POST['montant'];
        $idPatient=$_POST['idPatient'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->addRecette($idTypeActe,$dateActe,$montant,$idPatient);
        $this->utilRecette();
    }

    public function updateRecette(){
        $idActe=$_POST['idActe'];
        $idTypeActe=$_POST['idTypeActe'];
        $dateActe=$_POST['dateActe'];
        $montant=$_POST['montant'];
        $idPatient=$_POST['idPatient'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->updateActe($idActe,$idTypeActe,$dateActe,$montant,$idPatient);
        $this->utilRecette();
    }

    public function deleteRecette(){
        $idActe=$_GET['idActe'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->deleteRecette($idActe);
        $this->utilRecette();
    }

    public function csvToBase(){
        $this->load->model('Mon_Model');
        $filename=$_FILES["file"]["tmp_name"];
        $file=fopen($filename,"r");
        try {
            $this->Mon_Model->csvToBase($file);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $this->utilDepense();
    }


    public function facture($id=""){
        $idPatient=isset($_GET['idPatient']) ? $_GET['idPatient'] : $id;
        $data=array();
        $data['page']="facture";
        $this->load->model('Mon_Model');
        $patient=$this->Mon_Model->getPatientById($idPatient);
        $detail=$this->Mon_Model->getDetaitActePatient($idPatient);
        //$remboursement=$this->Mon_Model->getRemboursement($idPatient);
        $data['patient']=$patient;
        $data['detail']=$detail;
        $this->load->view('pageUtil',$data);
    }

    public function deleletDetailActePatient(){
        $idActe=$_GET['idActe'];
        $idPatient=$_GET['idPatient'];
        $this->load->model('Mon_Model');
        $this->Mon_Model->deleletDetailActePatient($idActe);
        $this->facture($idPatient);
    }

    // public function validerFacture(){
    //     $idPatient=$_POST['idPatient'];
    //     $this->load->Mon_Model();
    //     $this->Mon_Model->addFacture($idPatient);
    //     $facture=$this->Mon_Model->getLastFacture();
    //     if(isset($_POST['idActe'])) {
    //         $idActe=$_POST['idActe'];
    //         foreach ($idActe as $row ) {
    //             $this->Mon_Model->addDetailFacture($facture[0]['idfacture'],$row);
    //         }fjddb dohf
    //     }
    //     $this->utilRecette();
    // }

    public function  getPDF(){
        $idPatient=$_GET['idPatient'];
        $this->load->model('Mon_Model');
        $patient=$this->Mon_Model->getPatientById($idPatient);
        $this->Mon_Model->getPdf1($idPatient,$patient[0]['nom'],$patient[0]['prenom']);
    }
}
?>
