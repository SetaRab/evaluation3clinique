<section id="faq" class="faq section-bg">
<div class="container">
<div class="section-title" data-aos="fade-up">
          <h2>Patient</h2>
          <p>Liste des patient</p>
        </div>
              <!-- Default Table -->
              <div>
              <table class="table" style="margin-top: 20px;">
                <thead>
                  <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Prénom(s)</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php for ($i=0; $i <count($patient) ; $i++) { ?>
                  <tr>
                    <td><?php echo $patient[$i]['nom'] ?></td>
                    <td><?php echo $patient[$i]['prenom'] ?></td>
                    <td>              
                        <!-- Basic Modal -->
                        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" style="background-color: green;" data-bs-target="#basicModal<?php echo $i; ?>">
                        Ajout acte</button>
                        <div class="modal fade" id="basicModal<?php echo $i; ?>" tabindex="-1">
                            <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title">Ajouter un acte</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <center><h6><?php echo $patient[$i]['nom'] ?> <?php echo $patient[$i]['prenom'] ?></h6></center>
                                <form method="post" action="<?php echo site_url('Mon_Controlleur/addRecette'); ?>">
                                    <input type="hidden" name="idPatient" value="<?php echo $patient[$i]['idpatient'] ?>">
                                    <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Recette</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="idTypeActe" id="idTypeActe">
                                            <?php for ($j=0; $j <count($acte) ; $j++) { ?>
                                                <option value="<?php echo $acte[$j]['idtypeacte'] ?>"><?php echo $acte[$j]['nomacte'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputText" class="col-sm-2 col-form-label">Montant</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" name="montant" id="montant" >
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputText" class="col-sm-2 col-form-label">Date</label>
                                        <div class="col-sm-10">
                                            <input type="date" class="form-control" name="dateActe" id="dateActe" >
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;">Ajouter</button>
                                        </div>
                                    </div>
                                </form>                                
                                </div>
                            </div>
                            </div>
                        </div><!-- End Basic Modal--> 
                    </td>
                    <td><a href="<?php echo site_url('Mon_Controlleur/facture/'); ?>?idPatient=<?php echo $patient[$i]['idpatient'] ?>" class="btn btn-primary">Detail</a></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
        </section><!-- End F.A.Q Section -->


