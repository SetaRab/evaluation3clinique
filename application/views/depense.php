<section id="faq" class="faq section-bg">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Depense</h2>
          <p>Liste des depenses</p>
        </div>

        <div class="faq-list">
        <?php for ($i=0; $i <count($depense) ; $i++) { ?>
            <ul>
                <li data-aos="fade-up" data-aos-delay="100">
                    <i class="bx bx-help-circle icon-help"></i><a data-bs-toggle="collapse" class="collapsed"><?php echo $depense[$i]['nomdepense']?></a>
                    <p>Budget: <?php echo $depense[$i]['budget'] ?> Ar</p>
                    <p>Code: <?php echo $depense[$i]['code'] ?> </p>
                    <p>
                    <table>
                        <td><a class="btn btn" style=" background-color:  #1acc8d;" style="forced-color-adjust: green;" data-bs-toggle="modal" data-bs-target="#verticalycentered<?php echo $i?>"><i class="ri-edit-box-fill" ></i></a></td>
                        <td><a class="btn btn-danger"  data-bs-toggle="modal" data-bs-target="#myModal<?php echo $i?>"><i class="ri-delete-bin-5-fill" ></i></a></td>
                    </table>
                    </p>
                </li>
            </ul>
          <div class="modal fade" id="verticalycentered<?php echo $i?>" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Modifier une Dépense</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="<?php echo site_url('Mon_Controlleur/updateDepense'); ?>" >
                                <input type="hidden" name="idTypeDepense" value="<?php echo $depense[$i]['idtypedepense'] ?>">
                                  
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-5 col-form-label">Nom de la dépense</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="nomDepense" id="nomDepense" value="<?php echo $depense[$i]['nomdepense'] ?>">
                                    </div>
                                </div>
                                
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-5 col-form-label">Budget</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="budget" id="budget" value="<?php echo $depense[$i]['budget'] ?>">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-5 col-form-label">Code</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="code" id="code" maxlength="3" value="<?php echo $depense[$i]['code'] ?>">
                                    </div>
                                </div>


                                <div class="row mb-3">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;">Valider</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div> 


            <div class="modal" id="myModal<?php echo $i?>">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Supprimer</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Êtes-vous sûr de vouloir supprimer <?php echo $depense[$i]['nomdepense'] ?> ?
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Annuler</button>
                        <a href="<?php echo site_url("Mon_Controlleur/deleteDepense"); ?>?idTypeDepense=<?php echo $depense[$i]['idtypedepense']?>"><button type="button" class="btn btn-danger">Confirmer</button></a>
                    </div>

                    </div>
                </div>
            </div>


        <?php } ?>
        </div>

      </div>
    <center>
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#verticalycenter" style="width: 200px;">
            Ajouter une Dépense </button>
        <div class="modal fade" id="verticalycenter" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajout Dépense</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?php echo site_url('Mon_Controlleur/addDepense'); ?>" >
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-4 col-form-label">Nom de la dépense</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nomDepense" id="nomDepense" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-4 col-form-label">Budget</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="budget" id="budget" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                 <label for="inputText" class="col-sm-4 col-form-label">Code</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="code" id="code" maxlength="3" required>
                                </div>
                            </div>
                            
                            <div class="row mb-3">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;">Valider</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </center>
    </section><!-- End F.A.Q Section -->


