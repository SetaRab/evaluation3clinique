<section id="faq" class="faq section-bg">
<div class="container">
    <div class="section-title" data-aos="fade-up">
          <h2><?php echo $patient[0]['nom'] ?>  <?php echo $patient[0]['prenom'] ?></h2>
    </div>
    <div>
    <table class="table" style="margin-top: 20px;">
        <thead>
            <tr>
            <th scope="col">Désignation</th>
            <th scope="col">Date</th>
            <th scope="col">Montant</th>
            <th></th>
            <th></th>
            </tr>
        </thead>
        <tbody>
            <?php for ($i=0; $i <count($detail) ; $i++) { ?>
            <tr>
            <td><?php echo $detail[$i]['nomacte'] ?></td>
            <td><?php echo $detail[$i]['dateacte'] ?></td>
            <td><?php echo $detail[$i]['montant'] ?></td>
            <td>
            <div class="modal-footer">
                <a href="<?php echo site_url("Mon_Controlleur/deleletDetailActePatient"); ?>?idActe=<?php echo $detail[$i]['idacte']?>&idPatient=<?php echo $patient[0]['idpatient']?>"><button type="button" class="btn btn-danger">Delete</button></a>
            </div>
            </td>
            </tr>
            <?php } ?>
        </tbody>
        </table>
    </div>
    <center>
    <div class="">
        <a href="<?php echo site_url("Mon_Controlleur/getPDF"); ?>?idPatient=<?php echo $patient[0]['idpatient']?>"><button type="button" class="btn btn-primary">PDF</button></a>
    </div>
    </center>
</div>
</section><!-- End F.A.Q Section -->


