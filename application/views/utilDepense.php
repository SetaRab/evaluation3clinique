<section id="faq" class="faq section-bg">
<div class="container">
<div class="section-title" data-aos="fade-up">
          <h2>Dépense</h2>
          <p>Liste des dépenses</p>
        </div>
        <div>
            <?php
                echo $erreur;
            ?>
        </div>
              <!-- Default Table -->
              <div>
              <table class="table" style="margin-top: 20px;">
                <thead>
                  <tr>
                    <th scope="col">Désignation</th>
                    <th scope="col">Montant</th>
                    <th scope="col">Date</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php for ($i=0; $i <count($utilDepense) ; $i++) { ?>
                  <tr>
                    <td><?php echo $utilDepense[$i]['nomdepense'] ?></td>
                    <td><?php echo $utilDepense[$i]['montant'] ?></td>
                    <td><?php echo $utilDepense[$i]['datedepense'] ?></td>  
                    <td>              
                        <!-- Basic Modal -->
                        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#basicModal<?php echo $i; ?>">
                        <i class="ri-edit-box-fill"></i>
                        </button>
                        <div class="modal fade" id="basicModal<?php echo $i; ?>" tabindex="-1">
                            <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title">Modifier la dépense</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                <form method="post" action="<?php echo site_url('Mon_Controlleur/updateUtilDepense'); ?>">
                                    <input type="hidden" name="idDepense" value="<?php echo $utilDepense[$i]['iddepense'] ?>">
                                    <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Nom</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="idTypeDepense" id="idTypeDepense">
                                            <?php for ($j=0; $j <count($depense) ; $j++) { ?>
                                                <option value="<?php echo $depense[$j]['idtypedepense'] ?>"><?php echo $depense[$j]['nomdepense'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputText" class="col-sm-5 col-form-label">Date de Dépensse</label>
                                        <div class="col-sm-7    ">
                                            <input type="date" class="form-control" name="dateDepense" id="dateDepense" value="<?php echo $utilDepense[$i]['datedepense'] ?>">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputText" class="col-sm-2 col-form-label">Montant</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="montant" id="montant" value="<?php echo $utilDepense[$i]['montant'] ?>">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;">Modifier</button>
                                        </div>
                                    </div>
                                </form>                                
                                </div>
                            </div>
                            </div>
                        </div><!-- End Basic Modal--> 
                    <td>                
                                  <!-- Vertically centered Modal -->
              <a type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#verticalycentered<?php echo $i; ?>">
              <i class="ri-delete-bin-5-fill"></i>
                  </a>
              <div class="modal fade" id="verticalycentered<?php echo $i; ?>" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <form methodd="get" action="<?php echo site_url('Mon_Controlleur/deleteUtilDepense'); ?>">
                    <div class="modal-body">
                      <input type="hidden" name="idDepense" value="<?php echo $utilDepense[$i]['iddepense'] ?>">
                      Etes-vous sûr de vouloir supprimer <?php echo $utilDepense[$i]['nomdepense']?>?
                    </div>
                    <div class="modal-footer">
                      <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                      <button type="submit" class="btn btn-primary">Supprimer</button>
                    </div>
                  </form>
                  </div>
                </div>
              </div><!-- End Vertically centered Modal-->
              </td> 
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
    <center>
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#verticalycenter" style="width: 200px;">
            Ajouter une Dépense </button>
        <div class="modal fade" id="verticalycenter" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajout depense</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?php echo site_url('Mon_Controlleur/addUtilDepense'); ?>" >
                            <div class="row mb-3">
                                <div class="col-sm-6">
                                    <label for="inputText" class="col-sm-2 col-form-label">Typpe Dépense</label>
                                    <input type="text" maxlength="3" class="form-control" name="code" id="code" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-6">
                                    <label for="inputText" class="col-sm-2 col-form-label">Jour</label>
                                    <input type="number" maxlength="2" class="form-control" name="jour" id="jour" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="inputText" class="col-sm-2 col-form-label">Année</label>
                                    <input type="number" maxlength="4" class="form-control" name="annee" id="annee" required>
                                </div>
                            
                                <div class="col-sm-5">
                                    <?php for ($i=0; $i < 6; $i++) {  ?>
                                        <p><input type="checkbox" name="mois[]" value="<?php echo $mois[$i]['idmois'] ?>" ><?php echo $mois[$i]['mois'] ?></p>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php for ($i=6; $i < 12; $i++) {  ?>
                                        <p><input type="checkbox" name="mois[]" value="<?php echo $mois[$i]['idmois'] ?>" ><?php echo $mois[$i]['mois'] ?></p>
                                    <?php } ?>
                                </div>
                                
                                <div class="col-sm-15">
                                    <label for="inputText" class="col-sm-3 col-form-label">Montant</label>
                                    <input type="number"  class="form-control" name="montant" id="montant" required>
                                </div>
                            </div>
                            <div class="row mb-5">
                                <div class="col-sm-15">
                                    <button type="submit" class="btn btn-primary" style="width: 380px; ">Valider</button>
                                </div>
                            </div>

                        </form>
                        <form method="post" action="<?php echo site_url('Mon_Controlleur/csvToBase') ?>" enctype="multipart/form-data">
                            <div class="input-row">
                                <label class="col-md-4 control-label">Fichier CSV</label>
                                <input type="file"  name="file" id="file" accept=".csv">
                                <button type="submit" id="submit" class="btn btn-primary" name="import">Importer</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </center>
    </section><!-- End F.A.Q Section -->


