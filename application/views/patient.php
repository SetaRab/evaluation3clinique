<section id="faq" class="faq section-bg">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Patient</h2>
          <p>Liste des patients</p>
        </div>

        <div class="faq-list">
        <?php for ($i=0; $i <count($patient) ; $i++) { ?>
            <ul>
                <li data-aos="fade-up" data-aos-delay="100">
                    <i class="bx bx-help-circle icon-help"></i><a data-bs-toggle="collapse" class="collapsed"><?php echo $patient[$i]['nom']?> <?php echo $patient[$i]['prenom'] ?></a>
                    <p>Date de Naissance: <?php echo $patient[$i]['datenaissance'] ?></p>
                    <p>Genre: <?php echo $patient[$i]['genre'] ?></p>
                    <p>Contact: <?php echo $patient[$i]['contact'] ?></p>
                    <p>Rembourcement: <?php echo $patient[$i]['remboursement'] ?></p>
                    <table>
                        <td><a class="btn btn" style=" background-color:  #1acc8d;" style="forced-color-adjust: green;" data-bs-toggle="modal" data-bs-target="#verticalycentered<?php echo $i?>"><i class="ri-edit-box-fill" ></i></a></td>
                        <td><a class="btn btn-danger"  data-bs-toggle="modal" data-bs-target="#myModal<?php echo $i?>"><i class="ri-delete-bin-5-fill" ></i></a></td>
                    </table>
                </li>
            </ul>
          <div class="modal fade" id="verticalycentered<?php echo $i?>" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Modifier un patient</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="<?php echo site_url('Mon_Controlleur/updatePatient'); ?>" >
                                <input type="hidden" name="idPatient" value="<?php echo $patient[$i]['idpatient'] ?>">
                                  
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Nom</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="nom" id="nom" value="<?php echo $patient[$i]['nom'] ?>">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Prénom(s)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="prenom" id="prenom" value="<?php echo $patient[$i]['prenom'] ?>">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Date de naissance</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" name="datenaissance" id="datenaissance" value="<?php echo $patient[$i]['datenaissance'] ?>">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Genre</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="idGenre" id="idGenre">
                                        <?php for ($j=0; $j <count($genre) ; $j++) { ?>
                                            <option value="<?php echo $genre[$j]['idgenre'] ?>"><?php echo $genre[$j]['genre'] ?></option>
                                        <?php } ?>
                                    </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Contact</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="contact" id="contact" value="<?php echo $patient[$i]['contact'] ?>">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Remboursement</label>
                                    <div class="col-sm-10" style="padding-left: 20%;" >
                                    <select class="form-control" name="remboursement" id="remboursement" style="width: 100px;" >
                                        <option value="true">Oui</option>
                                        <option value="false">Non</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;">Valider</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div> 


            <div class="modal" id="myModal<?php echo $i?>">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Supprimer</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Êtes-vous sûr de vouloir supprimer <?php echo $patient[$i]['nom'] ?> <?php echo $patient[$i]['prenom'] ?> ?
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Annuler</button>
                        <a href="<?php echo site_url("Mon_Controlleur/deletePatient"); ?>?idPatient=<?php echo $patient[$i]['idpatient']?>"><button type="button" class="btn btn-danger">Confirmer</button></a>
                    </div>

                    </div>
                </div>
            </div>


        <?php } ?>
        </div>

      </div>
    <center>
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#verticalycenter" style="width: 200px;">
            Ajouter un Patient </button>
        <div class="modal fade" id="verticalycenter" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajout Patient</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?php echo site_url('Mon_Controlleur/addPatient'); ?>" >
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Nom</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="nom" id="nom" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Prénom(s)</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="prenom" id="prenom" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Date de naissance</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" name="datenaissance" id="datenaissance" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Genre</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="idGenre" id="idGenre">
                                        <?php for ($j=0; $j <count($genre) ; $j++) { ?>
                                            <option value="<?php echo $genre[$j]['idgenre'] ?>"><?php echo $genre[$j]['genre'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Contact</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="contact" id="contact" required>
                                </div>
                            </div><div class="row mb-3">
                                <label for="inputText" class="col-sm-2 col-form-label">Remboursement:</label>
                                <div>
                                <div class="col-sm-10">
                                    <select class="form-control" name="remboursement" id="remboursement">
                                        <option value="true">Oui</option>
                                        <option value="false">Non</option>
                                    </select>
                                </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;">Valider</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </center>
    </section><!-- End F.A.Q Section -->


