<section id="faq" class="faq section-bg">
<div class="container">
    <div class="section-title" data-aos="fade">
          <h2>Tableau De Bord</h2>
    </div>
    <div>
        <form action="<?php echo site_url('Mon_Controlleur/tableauDeBord'); ?>" method="get" >
            <div class="row mb-3">
                <label for="inputText" class="col-sm-2 col-form-label">Mois</label>
                <div class="col-sm-2">
                    <select class="form-control" name="idMois" id="idMois">
                    <?php for ($j=0; $j <count($mois) ; $j++) { ?>
                        <option></option>
                        <option value="<?php echo $mois[$j]['idmois'] ?>"><?php echo $mois[$j]['mois'] ?></option>
                    <?php } ?>
                </select>
                </div>
            </div>
            <div class="row mb-3">
                <label for="inputText" class="col-sm-2 col-form-label">Annee</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="annee" id="annee">
                </div>
            </div>
            
            <div class="row mb-1">
                <div class="col-sm-3">
                    <button type="submit" class="btn btn-primary">Recherche</button>
                </div>
            </div>
        </form>
    </div>

    <div class="section-title">
          <p>Recette</p>
    </div>
    <div>
        <table class="table" style="margin-top: 20px;">
            <thead>
            <tr>
                <th scope="col">Type Acte</th>
                <th scope="col">Réel</th>
                <th scope="col">Budget</th>
                <th scope="col">Réalisation</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i=0; $i <count($tabdebordrecette) ; $i++) { ?>
            <tr>
                <td><?php echo $tabdebordrecette[$i]['nomacte'] ?></td>
                <td><?php echo $tabdebordrecette[$i]['montanttotal'] ?></td>
                <td>
                    <?php   
                        $budgetAnnuelReccete=$tabdebordrecette[$i]['budget']/12;
                        echo sprintf("%.2f", $budgetAnnuelReccete);
                    ?>
                </td>
                <td><?php 
                    $reel=$tabdebordrecette[$i]['montanttotal'];
                    echo sprintf("%.2f",$resultat=($reel*100)/$budgetAnnuelReccete);
                ?> %</td>
            </tr>
            <?php } ?>
            <tr> 

                <td></td>
                <?php 
                $reelTotalRecette=0;
                $budgetTotalRecette=0;
                $resultatTotalRecette=0;
                for ($i=0; $i <count($tabdebordrecette) ; $i++) { 
                    $reelTotalRecette+=$tabdebordrecette[$i]['montanttotal'];
                    $budgetTotalRecette+=$tabdebordrecette[$i]['budget']/12;
                }?>
                <th><?php echo sprintf("%.2f",$reelTotalRecette) ?> </th>
                <th><?php echo sprintf("%.2f",$budgetTotalRecette) ?> </th>
                <th><?php echo sprintf("%.2f",$resultatTotalRecette=($reelTotalRecette*100)/$budgetTotalRecette); ?> %</th>
                <th></th>
                <th>
                    
                </th>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="section-title">
          <p>Dépense</p>
    </div>
    <div>
        <table class="table" style="margin-top: 20px;">
            <thead>
            <tr>
                <th scope="col">Type Acte</th>
                <th scope="col">Réel</th>
                <th scope="col">Budget</th>
                <th scope="col">Réalisation</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i=0; $i <count($tabdebord) ; $i++) { ?>
            <tr>
                <td><?php echo $tabdebord[$i]['nomdepense'] ?></td>
                <td><?php echo $tabdebord[$i]['montanttotal'] ?></td>
                <td>
                    <?php   
                        $budgetAnnuel=$tabdebord[$i]['budget']/12;
                        echo sprintf("%.2f", $budgetAnnuel);
                    ?>
                </td>
                <td><?php 
                    $reel=$tabdebord[$i]['montanttotal'];
                    echo sprintf("%.2f",$resultat=($reel*100)/$budgetAnnuel);
                ?> %</td>
            </tr>
            <?php } ?>
            <tr>

                <td></td>
                <?php 
                $reelTotal=0;
                $budgetTotal=0;
                $resultatTotal=0;
                for ($i=0; $i <count($tabdebord) ; $i++) { 
                    $reelTotal+=$tabdebord[$i]['montanttotal'];
                    $budgetTotal+=$tabdebord[$i]['budget']/12;
                }?>
                <th><?php echo sprintf("%.2f",$reelTotal) ?> </th>
                <th><?php echo sprintf("%.2f",$budgetTotal )?> </th>
                <th><?php echo sprintf("%.2f",$resultatTotal=($reelTotal*100)/$budgetTotal); ?> %</th>
                <th></th>
                <th>
                    
                </th>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="section-title">
          <p>Bénéfice</p>
    </div>
    <div>
        <table class="table" style="margin-top: 20px;">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Réel</th>
                <th scope="col">Budget</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Recette</td>
                <td><?php echo sprintf("%.2f",$reelTotalRecette) ?></td>
                <td><?php echo sprintf("%.2f",$budgetTotalRecette) ?></td>
                <td><?php echo sprintf("%.2f",$resultatTotalRecette) ?> % </td>
            </tr>
            <tr>
                <td>Dépense</td>
                <td><?php echo sprintf("%.2f",$reelTotal) ?></td>
                <td><?php echo sprintf("%.2f",$budgetTotal) ?></td>
                <td><?php echo sprintf("%.2f",$resultatTotal) ?> % </td>
            </tr>
            <tr>
                <td></td>
                <?php 
                    $reelTotalBenefice=$reelTotalRecette-$reelTotal;
                    $budgetTotalBenefice=$budgetTotalRecette-$budgetTotal;
                    $resultatTotalBenefice=($reelTotalBenefice*100)/$budgetTotalBenefice;
                ?>
                <th><?php echo sprintf("%.2f",$reelTotalBenefice) ?></th>
                <th><?php echo sprintf("%.2f",$budgetTotalBenefice) ?></th>
                <th><?php echo sprintf("%.2f",$resultatTotalBenefice) ?> % </th>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</section><!-- End F.A.Q Section -->


