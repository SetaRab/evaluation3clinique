<section id="faq" class="faq section-bg">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Actes</h2>
          <p>Liste des actes</p>
        </div>

        <div class="faq-list">
        <?php for ($i=0; $i <count($acte) ; $i++) { ?>
            <ul>
                <li data-aos="fade-up" data-aos-delay="100">
                    <i class="bx bx-help-circle icon-help"></i><a data-bs-toggle="collapse" class="collapsed"><?php echo $acte[$i]['nomacte']?></a>
                    <p>Budget: <?php echo $acte[$i]['budget'] ?> Ar</p>
                    <p>Code: <?php echo $acte[$i]['code'] ?> </p>
                    <p>
                    <table>
                        <td><a class="btn btn" style=" background-color:  #1acc8d;" style="forced-color-adjust: green;" data-bs-toggle="modal" data-bs-target="#verticalycentered<?php echo $i?>"><i class="ri-edit-box-fill" ></i></a></td>
                        <td><a class="btn btn-danger"  data-bs-toggle="modal" data-bs-target="#myModal<?php echo $i?>"><i class="ri-delete-bin-5-fill" ></i></a></td>
                    </table>
                    </p>
                </li>
            </ul>
          <div class="modal fade" id="verticalycentered<?php echo $i?>" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Modifier un Acte</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="<?php echo site_url('Mon_Controlleur/updateActe'); ?>" >
                                <input type="hidden" name="idTypeActe" value="<?php echo $acte[$i]['idtypeacte'] ?>">
                                  
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-5 col-form-label">Nom de l'acte</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="nom" id="nom" value="<?php echo $acte[$i]['nomacte'] ?>">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-5 col-form-label">Budget</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="budget" id="budget" value="<?php echo $acte[$i]['budget'] ?>">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-5 col-form-label">Code</label>
                                    <div class="col-sm-7">
                                        <input type="text" maxlength="3" class="form-control" name="code" id="code" value="<?php echo $acte[$i]['code'] ?>">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;">Valider</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div> 


            <div class="modal" id="myModal<?php echo $i?>">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Supprimer</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Êtes-vous sûr de vouloir supprimer <?php echo $acte[$i]['nomacte'] ?> ?
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Annuler</button>
                        <a href="<?php echo site_url("Mon_Controlleur/deleteActe"); ?>?idTypeActe=<?php echo $acte[$i]['idtypeacte']?>"><button type="button" class="btn btn-danger">Confirmer</button></a>
                    </div>

                    </div>
                </div>
            </div>


        <?php } ?>
        </div>

      </div>
    <center>
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#verticalycenter" style="width: 200px;">
            Ajouter un Acte </button>
        <div class="modal fade" id="verticalycenter" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ajout Acte</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?php echo site_url('Mon_Controlleur/addActe'); ?>" >
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-4 col-form-label">Nom de l'acte</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nomActe" id="nomActe" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputText" class="col-sm-4 col-form-label">Budget</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="budget" id="budget" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                 <label for="inputText" class="col-sm-4 col-form-label">Code</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="code" id="code" maxlength="3" required>
                                </div>
                            </div>
                            
                            <div class="row mb-3">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;">Valider</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </center>
    </section><!-- End F.A.Q Section -->


