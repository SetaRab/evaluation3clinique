<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
include('Liste_Compte.php');

class Mon_Model extends CI_Model{
	public function authentificate($email, $mdp){
		$query = "SELECT * FROM profil  where email = '".$email."' and mdp = '".$mdp."'";
        $result = $this->db->query($query);
        $array = $result->result_array();
        $rows = count($array);
        if ($rows == 1) {
            return $array;
        } else {
            return null;
        }
	}

	public function getGenre(){
		$query=$this->db->query("SELECT * FROM Genre");
		$genre=array();
		foreach ($query->result_array() as $row) {
			$genre[]=$row;
		}
		return $genre;
	}


	public function getPatient(){
		$query=$this->db->query("SELECT * FROM v_genrePatient where estsupprimer=0");
		$patient=array();
		foreach ($query->result_array() as $row) {
			$patient[]=$row;
		}
		return $patient;
	}

	public function getPatientById($idPatient){
		$query=$this->db->query("SELECT * FROM v_genrePatient where estsupprimer=0 and idPatient='".$idPatient."' ");
		$patient=array();
		foreach ($query->result_array() as $row) {
			$patient[]=$row;
		}
		return $patient;
	}

	public function addPatient($nom,$prenom,$datenaissance,$idGenre,$contact,$remboursement){
		if($nom!="" && $prenom!="" && $datenaissance!="" && $idGenre!="" && $contact!="" && $remboursement ){
			$sql="INSERT INTO Patient values (default,'".$nom."','".$prenom."', '".$datenaissance."','".$idGenre."','".$contact."','".$remboursement."');";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}
	
	public function updatePatient($idPatient,$nom,$prenom,$datenaissance,$idGenre,$contact,$remboursement){
		if($nom!="" && $prenom!="" && $datenaissance!="" && $idGenre!="" && $contact!="" && $remboursement ){
			$sql="UPDATE Patient SET nom='".$nom."', prenom='".$prenom."', datenaissance='".$datenaissance."', idGenre='".$idGenre."', contact='".$contact."', remboursement='".$remboursement."' WHERE idPatient='".$idPatient."'";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function deletePatient($idPatient){
		$sql="UPDATE Patient SET estSupprimer='1' where idPatient='".$idPatient."'";
		$sql=sprintf($sql);
		$sql=$this->db->query($sql);
	}

	public function getActe(){
		$query=$this->db->query("SELECT * FROM TypeActe where estSupprimer=0");
		$acte=array();
		foreach ($query->result_array() as $row) {
			$acte[]=$row;
		}
		return $acte;
	}

	public function addActe($nomActe,$budget,$code){
		if ($nomActe!="") {
			$sql="INSERT INTO TypeActe values (default,'".$nomActe."','".$budget."','".$code."')";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function updateActe($idTypeActe,$nomActe,$budget,$code){
		if ($nomActe!="" && $idTypeActe!="" && $budget!="" && $code!="") {
			$sql="UPDATE TypeActe SET nomActe='".$nomActe."', budget='".$budget."', code='".$code."' WHERE idTypeActe='".$idTypeActe."'";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function deleteActe($idTypeActe){
		$sql="UPDATE TypeActe SET estSupprimer='1' where idTypeActe='".$idTypeActe."'";
		$sql=sprintf($sql);
		$sql=$this->db->query($sql);
	}

	public function getDepense(){
		$query=$this->db->query("SELECT * FROM TypeDepense where estSupprimer=0");
		$acte=array();
		foreach ($query->result_array() as $row) {
			$acte[]=$row;
		}
		return $acte;
	}

	public function addDepense($nomDepense,$budget,$code){
		if ($nomDepense!="" && $budget!="" && $code!="") {
			$sql="INSERT INTO TypeDepense values (default,'".$nomDepense."','".$budget."','".$code."')";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function updateDepense($idTypeDepense,$nomDepense,$budget,$code){
		if ($nomDepense!="" && $idTypeDepense!="" && $budget!="" && $code!="") {
			$sql="UPDATE TypeDepense SET nomDepense='".$nomDepense."', budget='".$budget."', code='".$code."' where idTypeDepense='".$idTypeDepense."'";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function deleteDepense($idTypeDepense){
		$sql="UPDATE TypeDepense SET estSupprimer='1' where idTypeDepense='".$idTypeDepense."'";
		$sql=sprintf($sql);
		$sql=$this->db->query($sql);
	}


	public function getUtilDepense(){
		$query=$this->db->query("SELECT * FROM v_depense");
		$utildepense=array();
		foreach ($query->result_array() as $row) {
			$utildepense[]=$row;
		}
		return $utildepense;
	}

	public function addUtilDepense($idTypeDepense,$dateDepense,$montant){
		if ($idTypeDepense!="" && $dateDepense!="" && $montant!="") {
			$sql="INSERT INTO Depense values (default,".$idTypeDepense.",'".$dateDepense."','".$montant."') ";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function getIdCode($code){
		$query=$this->db->query("SELECT * FROM typedepense");
		$code=array();
		foreach ($query->result_array() as $row) {
			$code[]=$row;
		}
		return $code;
	}

	public function updateUtilDepense($idDepense,$idTypeDepense,$dateDepense,$montant){
		if ($idDepense!="" && $idTypeDepense!="" && $dateDepense!="" && $montant!="") {
			$sql="UPDATE Depense SET idTypeDepense='".$idTypeDepense."', dateDepense='".$dateDepense."', montant='".$montant."' where idDepense='".$idDepense."'";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function deleteUtilDepense($idDepense){
		$sql="DELETE FROM Depense where idDepense='".$idDepense."'";
		$sql=sprintf($sql);
		$sql=$this->db->query($sql);
	}


	public function getMois(){
		$query=$this->db->query("SELECT * FROM Mois");
		$mois=array();
		foreach ($query->result_array() as $row) {
			$mois[]=$row;
		}
		return $mois;
	}

	public function tableauDeBord($mois,$annee){
		$sql="SELECT * FROM v_depenseParMois WHERE ";
		if ($mois!="") {
			$sql=$sql."idMois='".$mois."' and ";
		}
		$sql=$sql."1=1 and ";
		if ($annee!="") {
			$sql=$sql."annee='".$annee."' and ";	
		}
		$sql=$sql."1=1 ";
		$query=$this->db->query($sql);
		$tabdebord=array();
		foreach ($query->result_array() as $row) {
			$tabdebord[]=$row;
		}
		return $tabdebord;
	}

	public function tableauDeBordRecette($mois,$annee){
		$sql="SELECT * FROM v_recetteParMois WHERE ";
		if ($mois!="") {
			$sql=$sql."idMois='".$mois."' and ";
		}
		$sql=$sql."1=1 and ";
		if ($annee!="") {
			$sql=$sql."annee='".$annee."' and ";	
		}
		$sql=$sql."1=1 ";
		$query=$this->db->query($sql);
		$tabdebord=array();
		foreach ($query->result_array() as $row) {
			$tabdebord[]=$row;
		}
		return $tabdebord;
	}


	public function getRecette(){
		$query=$this->db->query("SELECT * FROM Acte");
		$acte=array();
		foreach ($query->result_array() as $row) {
			$acte[]=$row;
		}
		return $acte;
	}

	public function addRecette($idTypeActe,$dateActe,$montant,$idPatient){
		if ($idTypeActe!="" && $dateActe!="" && $montant!="" && $idPatient!="") {
			$sql="INSERT INTO Acte values (default,'".$idTypeActe."','".$dateActe."','".$montant."','".$idPatient."')";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function updateRecette($idActe,$idTypeActe,$dateActe,$montant,$idPatient){
		if ($idActe!="" && $idTypeActe!="" && $dateActe!="" && $montant!="" && $idPatient!="") {
			$sql="UPDATE Acte SET idTypeActe='".$idTypeActe."', dateActe='".$dateActe."', montant='".$montant."', idPatient='".$idPatient."' WHERE idActe='".$idActe."'";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function deleteRecette($idActe){
		$sql="DELETE FROM Acte where idActe='".$idActe."'";
		$sql=sprintf($sql);
		$sql=$this->db->query($sql);
	}

	public function getTypeDepense($code){
		$query=$this->db->query("SELECT * FROM TypeDepense where estSupprimer=0 and code='".$code."'");
		$typedepense=array();
		foreach ($query->result_array() as $row) {
			$typedepense[]=$row;
		}
		return $typedepense;
	}
	public function csvToBase($csv){
		try {
			$column=fgetcsv($csv, 1000, ";");
			while (($column=fgetcsv($csv, 1000, ";"))!== FALSE) {
				$type=$this->getTypeDepense($column[1]);
				$sql="INSERT INTO Depense values (default,'%s','%s','%s')";
				$sql=sprintf($sql,$type[0]['idtypedepense'],$column[0],$column[2]);
				$val=$this->db->query($sql);
				if (! empty($val)) {
					$type = "OK";
					$message="Importation réussi";
				}else {
					throw new Exception("Error!!!");
				}
			}
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function addFacture($idPatient){
		if ($idPatient!="") {
			$sql="INSERT INTO Facture values (default,'".$idPatient."',now())";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function getLastFacture(){
		$query=$this->db->query("SELECT * FROM Facture order by idFacture desc");
		$lastFacture=array();
		foreach ($query->result_array() as $row) {
			$lastFacture[]=$row;
		}
		return $lastFacture;
	}

	public function addDetailFacture($idFacture,$idActe){
		if ($idFacture!="" && $idActe!="") {
			$sql="INSERT INTO DetailFacture values (default,'".$idFacture."','".$idActe."'";
			$sql=sprintf($sql);
			$sql=$this->db->query($sql);
		}
	}

	public function getFactureByPatient($idPatient){
		$query=$this->db->query("SELECT * FROM Facture where idPatient='".$idPatient."'");
		$val=array();
		foreach ($query->result_array() as $row) {
			$val[]=$row;
		}
		return $val;
	}

	public function getFactureByPatientAndFacture($idPatient,$idFacture){
		$query=$this->db->query("SELECT * FROM v_facture where idPatient='".$idPatient."' and idFacture='".$idFacture."' ");
		$val=array();
		foreach ($query->result_array() as $row) {
			$val[]=$row;
		}
		return $val;
	}

	public function getDetaitActePatient($idPatient){
		$query=$this->db->query("SELECT * FROM v_detailActePatient where idPatient='".$idPatient."'");
		$val=array();
		foreach ($query->result_array() as $row) {
			$val[]=$row;
		}
		return $val;
	}

	public function deleletDetailActePatient($idActe){
		$sql="DELETE FROM acte where idActe='".$idActe."'";
		$sql=sprintf($sql);
		$sql=$this->db->query($sql);
	}

	public function getPdf1($idPatient,$nom,$prenom){
	    $pdf = new Liste_Compte();
		$pdf->AddPage();
		$pdf->setFont('Arial','B',14);
		//$pdf->Cell(80);
		$pdf->Cell(80,10,'Facture de: '.$nom.' '.$prenom,0,1,'C');
        $liste = $this->getDetaitActePatient($idPatient);
        $data = array();
		$data[0][0]='Designation';
        $data[0][1]='Montant';
        $i=1;
		$total=0;
        foreach($liste as $list){
			$total=$total+$list['montant'];
			$data[$i][0]=$list['nomacte'];
            $data[$i][2]=$list['montant']."Ar";
            $i=$i+1;
        }
		$data[count($liste)+1][0]='Total';
        $data[count($liste)+1][1]=$total;
        $pdf->SetFont('Arial','',14);
        $pdf->BasicTable($data);
        $pdf->Output();
	}
	 public function getRemboursement($idPatient){
		$sql="SELECT * FROM Facture WHERE idPatient='".$idPatient."'";
		$sql=sprintf($sql);
		$sql=$this->db->query($sql);
		//topp
	 }

}
?>