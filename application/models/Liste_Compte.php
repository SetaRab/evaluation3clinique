<?php
require('fpdf.php');

class Liste_Compte extends FPDF
{
// Chargement des donn�es
function LoadData($file)
{
	// Lecture des lignes du fichier
	$lines = file($file);
	$data = array();
	foreach($lines as $line)
		$data[] = explode(';',trim($line));
	return $data;
}

// Tableau simple
function BasicTable($data)
{
	// En-t�te
	foreach($data[0] as $col)
		$this->Cell(70,7,$col,1);
	$this->Ln();
	// Donn�es
	for($i=1;$i<count($data);$i++)
	{
		foreach($data[$i] as $col)
			$this->Cell(70,6,$col,1);
		$this->Ln();
	}
}

// Tableau am�lior�
/*function ImprovedTable($header, $data)
{
	// Largeurs des colonnes
	$w = array(40, 35, 45, 40);
	// En-t�te
	for($i=0;$i<count($header);$i++)
		$this->Cell($w[$i],7,$header[$i],1,0,'C');
	$this->Ln();
	// Donn�es
	foreach($data as $row)
	{
		$this->Cell($w[0],6,$row[0],'LR');
		$this->Cell($w[1],6,$row[1],'LR');
		$this->Cell($w[2],6,number_format($row[2],0,',',' '),'LR',0,'R');
		$this->Cell($w[3],6,number_format($row[3],0,',',' '),'LR',0,'R');
		$this->Ln();
	}
	// Trait de terminaison
	$this->Cell(array_sum($w),0,'','T');
}*/

// Tableau color�
function FancyTable($data)
{
	// Couleurs, �paisseur du trait et police grasse
	$this->SetFillColor(255,0,0);
	$this->SetTextColor(255);
	$this->SetDrawColor(128,0,0);
	$this->SetLineWidth(.3);
	$this->SetFont('','B');
	// En-t�te
	$w = array(40, 130, 45, 40);
	for($i=0;$i<count($data[0]);$i++)
		$this->Cell($w[$i],7,$data[0][$i],1,0,'C',true);
	$this->Ln();
	// Restauration des couleurs et de la police
	$this->SetFillColor(224,235,255);
	$this->SetTextColor(0);
	$this->SetFont('');
	// Donn�es
	$fill = false;
	for($i=1;$i<count($data);$i++)
	{
		$this->Cell($w[0],6,$data[$i][0],'LR',0,'L',$fill);
		$this->Cell($w[1],6,$data[$i][1],'LR',0,'L',$fill);
		$this->Ln();
		$fill = !$fill;
	}
	// Trait de terminaison
	$this->Cell(array_sum($w),0,'','T');
}
}

?>
