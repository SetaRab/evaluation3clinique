/*create view v_depense as
select d.*, td.nomdepense from depense as d
join typedepense as td
on td.idTypeDepense=d.idTypeDepense;
create view v_montantAnneeParMois as
select extract(year from datedepense) as annee,extract(month from datedepense) as mois, sum(montant) as montantTotal
from v_depense 
group by annee,mois;
create view v_montantParMoisParAnnee as
select v.annee, Mois.mois, Mois.idMois, v.montantTotal from v_montantAnneeParMois as v
left join Mois 
on v.mois = Mois.idMois;*/




--------------------------------------------------------------------------------------------------------------------------------------------------------
/*

drop table if exists profil cascade;
drop table if exists patient cascade;
drop table if exists genre cascade;
drop table if exists typedepense cascade;
drop table if exists typeacte cascade;
drop table if exists depense cascade;
drop table if exists acte cascade;
drop table if exists mois cascade;
drop table if exists facture cascade;
drop table if exists detailfacture cascade;

*/

create table Profil(
    idProfil serial primary key,
    nom varchar(30),
    email varchar(30),
    mdp varchar(30),
    statut int
);
insert into Profil values
    (default,'admin','admin@gmail.com','root',0),
    (default,'utilisateur','utilisateur@gmail.com','root',1);

create table Genre(
    idGenre serial primary key,
    genre varchar(10)
);
insert into Genre values
    (default,'Homme'),
    (default,'Femme');

create table Patient(
    idPatient serial primary key,
    nom varchar(30),
    prenom varchar(50),
    dateNaissance date,
    idGenre int references genre(idGenre),
    contact varchar(20) unique,
    remboursement boolean,
    estSupprimer int default 0
);

create table TypeActe(
    idTypeActe serial primary key,
    nomActe varchar(50) unique,
    budget decimal(10,2),
    code varchar(255),
    estSupprimer int default 0
);

create table Acte(
    idacte serial primary key,
    idTypeActe int references TypeActe(idTypeActe),
    dateacte date,
    montant decimal(10,2)
);alter table Acte add column idPatient int references Patient(idPatient);

create table TypeDepense(
    idTypeDepense serial primary key,
    nomDepense varchar(50) unique,
    budget decimal(10,2),
    code varchar(255),
    estSupprimer int default 0 
);

create table Depense(
    idDepense serial primary key,
    idTypeDepense int references TypeDepense(idTypeDepense),
    dateDepense date,
    montant decimal(10,2)
);

create table Mois(
	idMois serial primary key,
	Mois varchar(100) not null unique
);
insert into Mois(Mois) values 
    ('Janvier'),
    ('Fevrier'),
    ('Mars'),
    ('Avril'),
    ('Mai'),
    ('Juin'),
    ('Juillet'),
    ('Aout'),
    ('Septembre'),
    ('Octobre'),
    ('Novembre'),
    ('Decembre');




create view v_genrePatient as
select p.*, g.genre from patient as p
join genre as g 
on g.idgenre = p.idgenre;


create view v_depense as
select d.*, t.nomdepense, extract(year from datedepense) as annee, extract(month from datedepense) as mois
from depense as d
join typedepense as t
on d.idtypedepense = t.idtypedepense;

create view v_recette as
select ac.*, ta.nomacte, extract(year from dateacte) as annee, extract(month from dateacte) as mois
from acte as ac
join typeacte as ta
on ac.idtypeacte = ta.idtypeacte;

create view v_montantMoisAnnee as
select annee, mois, sum(montant), nomdepense
from v_depense
group by annee,mois, nomdepense;

create view v_montantMoisAnneeRecette as
select annee, mois, sum(montant), nomacte
from v_recette
group by annee,mois, nomacte;

create view v_depenseAnnee as
select annee from v_montantMoisAnnee
group by annee;

create view v_recetteAnnee as
select annee from v_montantMoisAnneeRecette
group by annee;

create view v_depenseMoisAnnee as
select * from v_depenseAnnee
left join Mois 
on 1=1
left join TypeDepense
on 1=1
where estSupprimer=0
order by annee,idMois;

create view v_recetteMoisAnnee as
select * from v_recetteAnnee
left join Mois 
on 1=1
left join TypeActe
on 1=1
where estSupprimer=0
order by annee,idMois;

create view v_depenseParMois as
select dma.*, coalesce(sum, 0) as montanttotal
from v_depenseMoisAnnee as dma
left join v_montantMoisAnnee as mma
on dma.annee=mma.annee
and dma.idMois=mma.mois
and dma.nomdepense=mma.nomdepense;

create view v_recetteParMois as
select dma.*, coalesce(sum, 0) as montanttotal
from v_recetteMoisAnnee as dma
left join v_montantMoisAnneeRecette as mma
on dma.annee=mma.annee
and dma.idMois=mma.mois
and dma.nomacte=mma.nomacte;


create table Facture(
    idFacture serial primary key,
    idPatient int references Patient(idPatient),
    dateFacture date
);

create table DetailFacture(
    idDetailFacture serial primary key,
    idFacture int references Facture(idFacture),
    idActe int references TypeActe(idTypeActe)
);


create view v_facture as
select df.*, r.nomacte, r.dateacte, r.montant, f.dateFacture, f.idPatient, p.nom, p.prenom
from DetailFacture as df
join v_recette as r 
on df.idActe = r.idActe
join facture as f 
on df.idFacture=f.idFacture
join Patient as p 
on p.idPatient=f.idPatient;


create view v_detailActePatient as
select ac.*, ta.nomacte
from acte as ac
join typeacte as ta
on ac.idtypeacte = ta.idtypeacte;


-- create view v_depense as
-- select d.*, t.nomdepense, extract(year from datedepense) as annee, extract(month from datedepense) as mois
-- from depense as d
-- join typedepense as t
-- on d.idtypedepense = t.idtypedepense;

-- create view v_montantMoisAnnee as
-- select annee, mois, sum(montant)
-- from v_depense
-- group by annee,mois;

-- create view v_depenseAnnee as
-- select annee from v_montantMoisAnnee
-- group by annee;

-- create view v_depenseMoisAnnee as
-- select * from v_depenseAnnee
-- left join Mois 
-- on 1=1
-- order by annee,idMois;


-- create view v_depenseParMois as
-- select dma.*, coalesce(sum, 0) as montanttotal
-- from v_depenseMoisAnnee as dma
-- left join v_montantMoisAnnee as mma
-- on dma.annee=mma.annee
-- and dma.idMois=mma.mois;


select p.idPatient,p.nom,p.remboursement, vr.montant,
case
	when remboursement=true then montant*0.2
	else montant
	end as montantfin
from v_recette as vr
join Patient as p
on p.idPatient= vr.idPatient



create view v_codeType as
select * from Depense as d join typedepense as t on d.idtypedepense = t.idtypedepense 